{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "dab4c8f8",
   "metadata": {},
   "source": [
    "# Add an Image: Edit Data Gathering\n",
    "\n",
    "This notebook gathers edit data using MediaWiki history that allows us to answer question about our high level metrics: activation, retention, productivity, and revert proportions. It also gathers a dataset of \"highly active editors\" so we can answer that question as well."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "b22aca45",
   "metadata": {},
   "outputs": [],
   "source": [
    "import json\n",
    "import datetime as dt\n",
    "\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "\n",
    "from wmfdata import spark, mariadb"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "d0571177",
   "metadata": {},
   "outputs": [],
   "source": [
    "## Configuration variables\n",
    "\n",
    "## Start and end timestamps of user registrations from T311531-user-dataset.ipynb\n",
    "## Data gathering is automatically extended to 15 days beyond the end timestamp.\n",
    "exp_start_ts = dt.datetime(2022, 7, 4, 11, 50, 3)\n",
    "exp_end_ts = dt.datetime(2022, 10, 31, 11, 50, 3)\n",
    "\n",
    "\n",
    "## The wikis that we'll gather data for (from the above referenced notebook)\n",
    "wikis = ['arwiki', 'bnwiki', 'cswiki', 'frwiki', 'fawiki', 'ptwiki', 'trwiki']\n",
    "\n",
    "## The snapshot of mediawiki_history that we'll use\n",
    "mwh_snapshot = '2023-12'\n",
    "\n",
    "## The name of the table with the user dataset (from the above referenced notebook)\n",
    "canonical_user_table = 'nettrom_growth.addanimage_exp_users'\n",
    "\n",
    "## Filename of where the edit count dataset is stored.\n",
    "edit_data_output_filename = 'datasets/add-an-image-edit-data.tsv'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3e29232f",
   "metadata": {},
   "source": [
    "## Grabbing editing data\n",
    "\n",
    "We gather edit data in accordance with the key metrics for NEWTEA. Those are:\n",
    "\n",
    "* Editor activation\n",
    "* Editor retention\n",
    "* Average number of edits in the first two weeks after registration\n",
    "* Proportion of constructive edits (i.e. unreverted in 48 hours)\n",
    "\n",
    "This means we gather edit and revert counts so we can model activation, retention, and productivity with and without reverted edits as we see fit.\n",
    "\n",
    "Per NEWTEA we'll also separate edits by namespaces:\n",
    "\n",
    "1. All namespaces.\n",
    "2. Only Main and Talk (namespaces 0 and 1)\n",
    "3. All other namespaces.\n",
    "\n",
    "Since 2 and 3 are mutually exclusive, we'll count them separately and sum them up to get the first."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "6fde6ddd",
   "metadata": {},
   "outputs": [],
   "source": [
    "edit_data_query = '''\n",
    "WITH edits AS (\n",
    "    SELECT wiki_db, event_user_id AS user_id,\n",
    "    -- ns 0 & 1 edits on the first day\n",
    "    SUM(IF(page_namespace IN (0, 1)\n",
    "            AND unix_timestamp(event_timestamp, \"yyyy-MM-dd HH:mm:ss.S\") - \n",
    "                unix_timestamp(event_user_creation_timestamp, \"yyyy-MM-dd HH:mm:ss.S\") < 86400, 1, 0))\n",
    "        AS num_article_edits_24hrs,\n",
    "    -- ns 0 & 1 edits on the first day that were reverted\n",
    "    SUM(IF(page_namespace IN (0, 1)\n",
    "            AND revision_is_identity_reverted = true\n",
    "            AND revision_seconds_to_identity_revert < 60*60*48\n",
    "            AND unix_timestamp(event_timestamp, \"yyyy-MM-dd HH:mm:ss.S\") -\n",
    "                unix_timestamp(event_user_creation_timestamp, \"yyyy-MM-dd HH:mm:ss.S\") < 86400, 1, 0))\n",
    "        AS num_article_reverts_24hrs,\n",
    "    --  other namespace edits on the first day\n",
    "    SUM(IF(page_namespace NOT IN (0, 1)\n",
    "            AND unix_timestamp(event_timestamp, \"yyyy-MM-dd HH:mm:ss.S\") -\n",
    "                unix_timestamp(event_user_creation_timestamp, \"yyyy-MM-dd HH:mm:ss.S\") < 86400, 1, 0))\n",
    "        AS num_other_edits_24hrs,\n",
    "    -- other namespace reverts on the first day\n",
    "    SUM(IF(page_namespace NOT IN (0, 1)\n",
    "            AND revision_is_identity_reverted = true\n",
    "            AND revision_seconds_to_identity_revert < 60*60*48\n",
    "            AND unix_timestamp(event_timestamp, \"yyyy-MM-dd HH:mm:ss.S\") -\n",
    "                unix_timestamp(event_user_creation_timestamp, \"yyyy-MM-dd HH:mm:ss.S\") < 86400, 1, 0))\n",
    "        AS num_other_reverts_24hrs,\n",
    "    -- ns 0 & 1 edits on days 1–15\n",
    "    SUM(IF(page_namespace IN (0, 1)\n",
    "            AND unix_timestamp(event_timestamp, \"yyyy-MM-dd HH:mm:ss.S\") -\n",
    "                unix_timestamp(event_user_creation_timestamp, \"yyyy-MM-dd HH:mm:ss.S\") BETWEEN 86400 AND 15*86400, 1, 0))\n",
    "        AS num_article_edits_2w,\n",
    "    -- ns 0 & 1 edits on days 1–15 that were reverted\n",
    "    SUM(IF(page_namespace IN (0, 1)\n",
    "            AND revision_is_identity_reverted = true\n",
    "            AND revision_seconds_to_identity_revert < 60*60*48\n",
    "            AND unix_timestamp(event_timestamp, \"yyyy-MM-dd HH:mm:ss.S\") -\n",
    "                unix_timestamp(event_user_creation_timestamp, \"yyyy-MM-dd HH:mm:ss.S\") BETWEEN 86400 AND 15*86400, 1, 0))\n",
    "        AS num_article_reverts_2w,\n",
    "    -- other namespace edits on days 1–15\n",
    "    SUM(IF(page_namespace NOT IN (0, 1)\n",
    "            AND unix_timestamp(event_timestamp, \"yyyy-MM-dd HH:mm:ss.S\") -\n",
    "                unix_timestamp(event_user_creation_timestamp, \"yyyy-MM-dd HH:mm:ss.S\") BETWEEN 86400 AND 15*86400, 1, 0))\n",
    "        AS num_other_edits_2w,\n",
    "    -- other namespace reverts on days 1–15\n",
    "    SUM(IF(page_namespace NOT IN (0, 1)\n",
    "            AND revision_is_identity_reverted = true\n",
    "            AND revision_seconds_to_identity_revert < 60*60*48\n",
    "            AND unix_timestamp(event_timestamp, \"yyyy-MM-dd HH:mm:ss.S\") -\n",
    "                unix_timestamp(event_user_creation_timestamp, \"yyyy-MM-dd HH:mm:ss.S\") BETWEEN 86400 AND 15*86400, 1, 0))\n",
    "        AS num_other_reverts_2w\n",
    "    FROM wmf.mediawiki_history\n",
    "    WHERE snapshot = \"{snapshot}\"\n",
    "    AND event_entity = \"revision\"\n",
    "    AND event_type = \"create\"\n",
    "    AND wiki_db IN ({wiki_list})\n",
    "    AND event_timestamp > \"{start_date}\"\n",
    "    AND event_timestamp < \"{end_date}\"\n",
    "    GROUP BY wiki_db, event_user_id\n",
    "),\n",
    "users AS (\n",
    "    SELECT\n",
    "        wiki_db,\n",
    "        user_id,\n",
    "        user_registration_timestamp,\n",
    "        reg_on_mobile,\n",
    "        hp_enabled,\n",
    "        hp_variant\n",
    "    FROM {exp_user_table}\n",
    ")\n",
    "SELECT\n",
    "    users.wiki_db,\n",
    "    users.user_id,\n",
    "    users.user_registration_timestamp,\n",
    "    users.reg_on_mobile,\n",
    "    users.hp_enabled,\n",
    "    users.hp_variant,\n",
    "    COALESCE(num_article_edits_24hrs, 0) AS num_article_edits_24hrs,\n",
    "    COALESCE(num_article_reverts_24hrs, 0) AS num_article_reverts_24hrs,\n",
    "    COALESCE(num_other_edits_24hrs, 0) AS num_other_edits_24hrs,\n",
    "    COALESCE(num_other_reverts_24hrs, 0) AS num_other_reverts_24hrs,\n",
    "    COALESCE(num_article_edits_2w, 0) AS num_article_edits_2w,\n",
    "    COALESCE(num_article_reverts_2w, 0) AS num_article_reverts_2w,\n",
    "    COALESCE(num_other_edits_2w, 0) AS num_other_edits_2w,\n",
    "    COALESCE(num_other_reverts_2w, 0) AS num_other_reverts_2w\n",
    "FROM users\n",
    "LEFT JOIN edits\n",
    "ON users.wiki_db = edits.wiki_db\n",
    "AND users.user_id = edits.user_id\n",
    "'''"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "9d771adb",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "SPARK_HOME: /usr/lib/spark3\n",
      "Using Hadoop client lib jars at 3.2.0, provided by Spark.\n",
      "PYSPARK_PYTHON=/opt/conda-analytics/bin/python3\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Setting default log level to \"WARN\".\n",
      "To adjust logging level use sc.setLogLevel(newLevel). For SparkR, use setLogLevel(newLevel).\n",
      "24/02/02 23:30:21 WARN SparkConf: Note that spark.local.dir will be overridden by the value set by the cluster manager (via SPARK_LOCAL_DIRS in mesos/standalone/kubernetes and LOCAL_DIRS in YARN).\n",
      "24/02/02 23:30:35 WARN YarnSchedulerBackend$YarnSchedulerEndpoint: Attempted to request executors before the AM has registered!\n",
      "24/02/02 23:30:39 WARN SessionState: METASTORE_FILTER_HOOK will be ignored, since hive.security.authorization.manager is set to instance of HiveAuthorizerFactory.\n",
      "                                                                                \r"
     ]
    }
   ],
   "source": [
    "## We set the start date to the experiment start date,\n",
    "## and the end date to the experiment end date + 15 days\n",
    "## to give users who registered within 15 days of the last\n",
    "## date the same amount of time to edit as everyone else.\n",
    "\n",
    "all_users_edit_data = spark.run(\n",
    "    edit_data_query.format(\n",
    "        snapshot = mwh_snapshot,\n",
    "        wiki_list = ','.join(['\"{}\"'.format(w) for w in wikis]),\n",
    "        start_date = exp_start_ts.date().isoformat(),\n",
    "        end_date = (exp_end_ts.date() + dt.timedelta(days = 15)).isoformat(),\n",
    "        exp_user_table = canonical_user_table\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "1c1b93f1",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "148419"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "len(all_users_edit_data)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c678d19d",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_users_edit_data.loc[all_users_edit_data['num_article_edits_24hrs'] > 0].head()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e5c5bd40",
   "metadata": {},
   "source": [
    "Write out the canonical edit dataset for importing into R."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "e0847589",
   "metadata": {},
   "outputs": [],
   "source": [
    "all_users_edit_data.to_csv(edit_data_output_filename,\n",
    "                           header = True, index = False, sep = '\\t')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
